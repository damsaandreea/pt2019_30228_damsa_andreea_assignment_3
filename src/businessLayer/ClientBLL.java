package businessLayer;

import java.util.List;
import java.util.NoSuchElementException;

import com.mysql.jdbc.exceptions.MySQLIntegrityConstraintViolationException;

import dataAccessLayer.ClientDAO;
import model.Client;
import model.Produs;

public class ClientBLL {
	private ClientDAO clientDAO;
	public ClientBLL() {
		clientDAO=new ClientDAO();
	}
	public Client findById(int id) {
		Client client=clientDAO.findById(id);
		if (client==null) {
			throw new NoSuchElementException("The client with id="+id+" was not found");
		}
		return client;
	}
	public List<Client> findAll() {
		List<Client> clients=clientDAO.findAll();
		return clients;
	}
	public int maxID() {
		List<Client> clients=clientDAO.findAll();
		return clients.size();
	}
	public Client insert(Client c) {
		Client cl;
		 cl=clientDAO.insert(c);
		return cl;
	}
	public Client update(Client c) {
		Client cl=clientDAO.update(c);
		return cl;
	}
	public Client delete(Client c) {
		Client cl=clientDAO.delete(c);
		return cl;
	}
	

}
