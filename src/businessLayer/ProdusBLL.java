package businessLayer;

import java.util.List;
import java.util.NoSuchElementException;

import dataAccessLayer.ProdusDAO;
import model.Produs;

public class ProdusBLL {
	private ProdusDAO produsDAO;
	public ProdusBLL() {
		produsDAO=new ProdusDAO();
	}
	public Produs findProdusById(int id) {
		Produs pr=produsDAO.findById(id);
		//System.out.println(pr.toString());
		if (pr == null) {
			throw new NoSuchElementException("The product with id =" + id + " was not found!");
		}
		return pr;
		
	}
	public List<Produs> findAll() {
		List<Produs> products=produsDAO.findAll();
		return products;
	}
	public int maxID() {
		List<Produs> products=produsDAO.findAll();
		int n=products.size();
		//System.out.println(n);
		return n;
	}
	public Produs insert(Produs p) {
		Produs pr=produsDAO.insert(p);
		return pr;
	}
	public Produs update(Produs p) {
		Produs pr=produsDAO.update(p);
		return pr;
	}
	public Produs delete(Produs p) {
		Produs pr=produsDAO.delete(p);
		return pr;
	}

}
