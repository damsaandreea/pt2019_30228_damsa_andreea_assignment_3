package businessLayer;

import java.util.List;
import java.util.NoSuchElementException;

import dataAccessLayer.ComandaDAO;
import model.Comanda;
import model.Produs;

public class ComandaBLL {
	ComandaDAO comandaDAO;
	public ComandaBLL(){
		comandaDAO=new ComandaDAO();
	}
	public Comanda findById(int id) {
		Comanda com=comandaDAO.findById(id);
		if (com==null) {
			throw new NoSuchElementException("The order with id="+id+"does not exist");
		}
		return com;
	}
	public List<Comanda> findAll() {
		List<Comanda> comenzi=comandaDAO.findAll();
		return comenzi;
	}
	
	public Comanda insert(Comanda p) {
		
		Comanda com=comandaDAO.insert(p);
		return com;
	}

}
