package presentation;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.List;

import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;

import businessLayer.ProdusBLL;
import model.Produs;

public class TabelProduse {
	private ProdusBLL produsBll;
	JFrame t=new JFrame();
	JButton ok=new JButton("OK");
	public TabelProduse(ProdusBLL produsBll) {
		this.produsBll=produsBll;
		List<Produs> lista=this.produsBll.findAll();
		String[] column= {"ID", "Nume","Cantitate", "Pret", "Perisabil"};
		int size=lista.size();
		String[][] data= new String[size][20];
		int index=0;
		for( Produs p:lista ) {
			data[index][0]=Integer.toString(p.getId());
			data[index][1]=p.getNume();
			data[index][2]=Integer.toString(p.getCantitate());
			data[index][3]=Integer.toString(p.getPret());
			data[index][4]=p.getPerisabil();
			index++;
		}
		JTable table=new JTable(data,column);    
	    table.setBounds(30,40,200,200);          
	    JScrollPane sp=new JScrollPane(table);
	    sp.setSize(200,200);
	    JPanel content=new JPanel();
	    content.setLayout(new BoxLayout(content, BoxLayout.Y_AXIS));
		content.add(sp);
		content.add(ok);
		ok.addActionListener(new OKActionListener());
		t.setContentPane(content);
		t.setSize(500,400);
		t.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		t.setTitle("Produse");
		t.setVisible(true);
	}
	public class OKActionListener implements ActionListener{
		public void actionPerformed(ActionEvent e) {
			t.setVisible(false);
		}
	}
	

}
