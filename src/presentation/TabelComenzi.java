package presentation;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.List;

import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;

import businessLayer.ClientBLL;
import businessLayer.ComandaBLL;
import model.Client;
import model.Comanda;
import presentation.TabelClienti.OKActionListener;

public class TabelComenzi {
	private ComandaBLL comandaBll;
	JFrame t=new JFrame();
	JButton ok=new JButton("OK");
	public TabelComenzi(ComandaBLL comandaBll) {
		this.comandaBll=comandaBll;
		List<Comanda> lista=this.comandaBll.findAll();
		String[] column= {"ID", "Client","Produs", "Cantitate"};
		int size=lista.size();
		String[][] data= new String[size][20];
		int index=0;
		if (lista!=null) {
		for( Comanda c:lista ) {
			data[index][0]=Integer.toString(c.getId());
			data[index][1]=c.getClient();
			data[index][2]=c.getProdus();
			data[index][3]=Integer.toString(c.getCantitate());
			index++;
		}}
		JTable table=new JTable(data,column);    
	    table.setBounds(60,100,900,500);      
	    JScrollPane sp=new JScrollPane(table);
	    sp.setSize(600,600);
	    t.setSize(600, 600);
	    JPanel content=new JPanel();
	    content.setLayout(new BoxLayout(content, BoxLayout.Y_AXIS));
		content.add(sp);
		content.add(ok);
		ok.addActionListener(new OKActionListener());
		t.setContentPane(content);
		t.setSize(500,400);
		t.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		t.setTitle("Produse");
		t.setVisible(true);
	}
	public class OKActionListener implements ActionListener{
		public void actionPerformed(ActionEvent e) {
			t.setVisible(false);
		}
	}
}
