package presentation;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.List;

import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;

import businessLayer.ClientBLL;
import model.Client;
import model.Produs;
import presentation.TabelProduse.OKActionListener;

public class TabelClienti {
	private ClientBLL clientBll;
	JFrame t=new JFrame();
	JButton ok=new JButton("OK");
	public TabelClienti(ClientBLL clientBll) {
		this.clientBll=clientBll;
		List<Client> lista=this.clientBll.findAll();
		String[] column= {"ID", "Nume","Adresa", "Email", "Varsta"};
		int size=lista.size();
		String[][] data= new String[size][20];
		int index=0;
		for( Client c:lista ) {
			data[index][0]=Integer.toString(c.getId());
			data[index][1]=c.getNume();
			data[index][2]=c.getAdresa();
			data[index][3]=c.getEmail();
			data[index][4]=Integer.toString(c.getVarsta());
			index++;
		}
		JTable table=new JTable(data,column);    
	    table.setBounds(60,100,900,500);      
	    JScrollPane sp=new JScrollPane(table);
	    sp.setSize(600,600);
	    t.setSize(600, 600);
	    JPanel content=new JPanel();
	    content.setLayout(new BoxLayout(content, BoxLayout.Y_AXIS));
		content.add(sp);
		content.add(ok);
		ok.addActionListener(new OKActionListener());
		t.setContentPane(content);
		t.setSize(500,400);
		t.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		t.setTitle("Produse");
		t.setVisible(true);
	}
	public class OKActionListener implements ActionListener{
		public void actionPerformed(ActionEvent e) {
			t.setVisible(false);
		}
	}

}
