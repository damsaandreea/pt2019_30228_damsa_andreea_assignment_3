package presentation;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import com.mysql.jdbc.exceptions.MySQLIntegrityConstraintViolationException;

import businessLayer.ClientBLL;
import businessLayer.ComandaBLL;
import businessLayer.ProdusBLL;
import model.Client;
import model.Comanda;
import model.Produs;

public class Controller {
	private ClientBLL clientBll;
	private ProdusBLL produsBll;
	private ComandaBLL comandaBll;
	private View view;
	public Controller(ClientBLL clientBll, ProdusBLL produsBll, ComandaBLL comandaBll, View view) {
		this.clientBll=clientBll;
		this.produsBll=produsBll;
		this.comandaBll=comandaBll;
		this.view=view;
		//adaugam scltatori la vedere
		view.adaugaCListener(new AdaugaCListener());
		view.editeazaCListener(new EditeazaCListener());
		view.stergeCListener(new StergeCListener());
		view.afiseazaCListener(new AfiseazaCListener());
		view.adaugaPListener(new AdaugaPListener());
		view.editeazaPListener(new EditeazaPListener());
		view.stergePListener(new StergePListener());
		view.afiseazaPListener(new AfiseazaPListener());
		view.afiseazaComenziListener(new AfiseazaComenziListener());
		view.adaugaComListener(new AdaugaComListener());
	}
	
	public class AdaugaCListener implements ActionListener{
		public void actionPerformed(ActionEvent e){
			String id=view.getIdC();
			String nume=view.getNumeC();
			String adresa=view.getAdresaC();
			String email=view.getEmailC();
			String varsta=view.getVarstaC();
			Client c=new Client(Integer.parseInt(id),nume,adresa,email,Integer.parseInt(varsta));
			clientBll.insert(c);
			view.showMessage("Inserare facuta cu succes!");
			}
		}
	public class EditeazaCListener implements ActionListener{
		public void actionPerformed(ActionEvent e) {
			int max=clientBll.maxID();
			String id=view.getIdC();
			String nume=view.getNumeC();
			String adresa=view.getAdresaC();
			String email=view.getEmailC();
			String varsta=view.getVarstaC();
			Client c=new Client(Integer.parseInt(id),nume,adresa,email,Integer.parseInt(varsta));
			try {
				if (Integer.parseInt(id)>max)
					throw new Exception("Nu exista client cu acest ID!");
			clientBll.update(c);
			view.showMessage("Editare facuta cu succes!");}
			catch(Exception exec) {
				view.showMessage(exec.getMessage());
			}
		}
	}
	public class StergeCListener implements ActionListener{
		public void actionPerformed(ActionEvent e) {
			int max=clientBll.maxID();
			String id=view.getIdC();
			Client c=new Client(Integer.parseInt(id),"notRelevat","notRelevat","notRelevat",20);
			try {
				if (Integer.parseInt(id)>max)
					throw new Exception("Nu exista client cu acest ID!");
			clientBll.delete(c);
			view.showMessage("Editare facuta cu succes!");}
			catch(Exception exec) {
				view.showMessage(exec.getMessage());
			}
			view.showMessage("Stergere facuta cu succes!");
		}
	}
	public class AfiseazaCListener implements ActionListener{
		public void actionPerformed(ActionEvent e) {
			TabelClienti tc=new TabelClienti(clientBll);
		}
	}
	public class AdaugaPListener implements ActionListener{
		public void actionPerformed(ActionEvent e) {
			String id=view.getIdP();
			String nume=view.getNumeP();
			String cantitate=view.getCantitateP();
			String pret=view.getPretP();
			String perisabil=view.getPerisabilP();
			Produs p=new Produs(Integer.parseInt(id),nume, Integer.parseInt(cantitate),Integer.parseInt(pret),perisabil);
			produsBll.insert(p);
			view.showMessage("Inserare facuta cu succes!");
		}
	}
	public class EditeazaPListener implements ActionListener{
		public void actionPerformed(ActionEvent e) {
			int max=produsBll.maxID();
			String id=view.getIdP();
			String nume=view.getNumeP();
			String cantitate=view.getCantitateP();
			String pret=view.getPretP();
			String perisabil=view.getPerisabilP();
			Produs p=new Produs(Integer.parseInt(id),nume, Integer.parseInt(cantitate),Integer.parseInt(pret),perisabil);
			try {
				if (Integer.parseInt(id)>max)
					throw new Exception("Acest ID nu exista in baza de date!");
			produsBll.update(p);
			view.showMessage("Editare facuta cu succes!");}
			catch(Exception excep) {
				view.showMessage(excep.getMessage());
			}
		}
	}
	public class StergePListener implements ActionListener{
		public void actionPerformed(ActionEvent e) {
			int max=produsBll.maxID();
			String id=view.getIdP();
			Produs p=new Produs(Integer.parseInt(id),"notRelevant",0,0,"nu");
			try {
				if (Integer.parseInt(id)>max)
					throw new Exception("Acest ID nu exista in baza de date!");
			produsBll.delete(p);
			view.showMessage("Stergere facuta cu succes!");
			view.showMessage("Editare facuta cu succes!");}
			catch(Exception excep) {
				view.showMessage(excep.getMessage());
			}
		}
	}
	public class AfiseazaPListener implements ActionListener{
		public void actionPerformed(ActionEvent e) {
			TabelProduse tab=new TabelProduse(produsBll);
		}
	}
	public class AfiseazaComenziListener implements ActionListener{
		public void actionPerformed(ActionEvent e) {
			TabelComenzi tc= new TabelComenzi(comandaBll);
		}
	}
	public class AdaugaComListener implements ActionListener{
		public void actionPerformed(ActionEvent e) {
			int client=view.getComboC();
			int produs=view.getComboP();		
			int cantitate=Integer.parseInt(view.getCantitate());
			Client c=clientBll.findById(client);
			Produs p=produsBll.findProdusById(produs);
			try {
			Comanda com=new Comanda(c.getNume(), p.getNume(), cantitate);
			if (cantitate> p.getCantitate())
				throw new Exception("Nu exista suficient stoc");
			comandaBll.insert(com);
			p.setCantitate(p.getCantitate()-cantitate);
			produsBll.update(p);
			view.showMessage("Comanda a fost creeata cu succes!");
			}catch(Exception except) {
				view.showMessage(except.getMessage());
			}
			
		}
	}
	
}
