package presentation;
import java.util.List;

import java.awt.event.ActionListener;

import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;

import businessLayer.ClientBLL;
import businessLayer.ProdusBLL;
import model.Client;
import model.Produs;
public class View extends JFrame{
	private ProdusBLL produsBll;
	private ClientBLL clientBll;
	private JButton adaugaC= new JButton ("Adauga");
	private JButton editeazaC=new JButton ("Editeaza");
	private JButton stergeC=new JButton ("Sterge");
	private JButton afiseazaC=new JButton("Afiseaza");
	private JTextField idC=new JTextField(10);
	private JTextField numeC=new JTextField(20);
	private JTextField adresaC=new JTextField(20);
	private JTextField emailC=new JTextField(20);
	private JTextField varstaC=new JTextField(10);
	private JButton adaugaP= new JButton ("Adauga");
	private JButton editeazaP=new JButton ("Editeaza");
	private JButton stergeP=new JButton ("Sterge");
	private JButton afiseazaP=new JButton("Afiseaza");
	private JTextField idP=new JTextField(10);
	private JTextField numeP=new JTextField(20);
	private JTextField cantitateP= new JTextField(10);
	private JTextField pretP= new JTextField(10);
	private JTextField perisabilP=new JTextField(5);
	private JTextField cantitate=new JTextField(5);
	private JButton afiseazaComenzi= new JButton("Afiseaza");
	private JButton adaugaComanda=new JButton("Creeaza");
	JComboBox<Integer> clientsList;
	JComboBox<Integer> productsList;
	public View(ProdusBLL produsBll, ClientBLL clientBll) {
		this.produsBll=produsBll;
		this.clientBll=clientBll;
		List<Produs> listap=produsBll.findAll();
		Integer[] combop=new Integer[listap.size()];
		int index=0;
		for (Produs p: listap) {
			combop[index]=p.getId();
			index++;
		}
		productsList= new JComboBox<Integer>(combop);
		List<Client> listac=clientBll.findAll();
		Integer[] comboc=new Integer[listac.size()];
		index=0;
		for (Client c:listac) {
			comboc[index]=c.getId();
			index++;
		}
		clientsList=new JComboBox<Integer>(comboc);
		//cele 3 JPaneluri principale
		JPanel total = new JPanel();
		JPanel client=new JPanel();
		JPanel produs=new JPanel();
		JPanel comanda=new JPanel();
		JLabel opC=new JLabel("Operatii Clienti:");
		JPanel butoaneC=new JPanel();
		butoaneC.add(adaugaC);
		butoaneC.add(editeazaC);
		butoaneC.add(stergeC);
		butoaneC.add(afiseazaC);
		JLabel infoC= new JLabel("Introduceti datele despre client(pentru stergere este suficient id):");
		JPanel caseteC= new JPanel();
		caseteC.add(new JLabel("ID:"));
		caseteC.add(idC);
		caseteC.add(new JLabel("Nume:"));
		caseteC.add(numeC);
		caseteC.add(new JLabel("Adresa:"));
		caseteC.add(adresaC);
		caseteC.add(new JLabel("Email:"));
		caseteC.add(emailC);
		caseteC.add(new JLabel("Varsta:"));
		caseteC.add(varstaC);
		caseteC.setLayout(new BoxLayout(caseteC, BoxLayout.Y_AXIS));
		client.setLayout(new BoxLayout(client, BoxLayout.Y_AXIS));
		client.add(opC);
		client.add(butoaneC);
		client.add(infoC);
		client.add(caseteC);
		JLabel opP=new JLabel("Operatii Produse:");
		JPanel butoaneP=new JPanel();
		butoaneP.add(adaugaP);
		butoaneP.add(editeazaP);
		butoaneP.add(stergeP);
		butoaneP.add(afiseazaP);
		JLabel infoP= new JLabel ("Introduceti datele despre produs(pentru stergere este suficient id):");
		JPanel caseteP= new JPanel();
		caseteP.add(new JLabel("ID:"));
		caseteP.add(idP);
		caseteP.add(new JLabel("Nume:"));
		caseteP.add(numeP);
		caseteP.add(new JLabel("Cantitate:"));
		caseteP.add(cantitateP);
		caseteP.add(new JLabel("Pret:"));
		caseteP.add(pretP);
		caseteP.add(new JLabel("Perisabil:"));
		caseteP.add(perisabilP);
		caseteP.setLayout(new BoxLayout(caseteP, BoxLayout.Y_AXIS));
		produs.setLayout(new BoxLayout(produs, BoxLayout.Y_AXIS));
		produs.add(opP);
		produs.add(butoaneP);
		produs.add(infoP);
		produs.add(caseteP);
		JLabel com=new JLabel("Creare comada:");
		comanda.add(com);
		comanda.add(new JLabel("Client"));
		comanda.add(clientsList);
		comanda.add(new JLabel("Produs disponibil:"));
		comanda.add(productsList);
		comanda.add(new JLabel("Cantitate:"));
		comanda.add(cantitate);
		comanda.add(adaugaComanda);
		comanda.add(afiseazaComenzi);
		JPanel semi=new JPanel();
		semi.add(client);
		semi.add(produs);
		total.add(semi);
		total.add(comanda);
		total.setLayout(new BoxLayout(total,BoxLayout.Y_AXIS));
		this.setContentPane(total);
		this.setVisible(true); 
		this.setSize(1200, 600);
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		this.setTitle("Depozit");
		}
	//ascultatori pentru butoane 
	public void adaugaComListener(ActionListener a) {
		adaugaComanda.addActionListener(a);
	}
	public void afiseazaComenziListener(ActionListener a) {
		afiseazaComenzi.addActionListener(a);
	}
	public void adaugaCListener(ActionListener a) {
		adaugaC.addActionListener(a);
		}
	public void editeazaCListener(ActionListener a) {
		editeazaC.addActionListener(a);
	}
	public void stergeCListener(ActionListener a) {
		stergeC.addActionListener(a);
	}
	public void afiseazaCListener(ActionListener a)
	{
		afiseazaC.addActionListener(a);
	}
	public void adaugaPListener(ActionListener a) {
		adaugaP.addActionListener(a);
	}
	public void editeazaPListener(ActionListener a) {
		editeazaP.addActionListener(a);
	}
	public void stergePListener(ActionListener a) {
		stergeP.addActionListener(a);
	}
	public void afiseazaPListener(ActionListener a) {
		afiseazaP.addActionListener(a);
	}
	//setere
	public void setIdC(String s) {
		idC.setText(s);
	}
	public void setNumeC(String s)
	{
		numeC.setText(s);
	}
	public void setAdresaC(String s) {
		adresaC.setText(s);
	}
	public void setEmailC(String s) {
		emailC.setText(s);
	}
	public void setVarstaC(String s) {
		varstaC.setText(s);
	}
	public void setIdP(String s) {
		idP.setText(s);
	}
	public void setNumeP(String s) {
		numeP.setText(s);
	}
	public void setCantitateP(String s) {
		cantitateP.setText(s);
	}
	public void setPerisabilP(String s) {
		perisabilP.setText(s);
	}
	//getere
	public String getIdC() {
		return idC.getText();
	}
	public String getNumeC() {
		return numeC.getText();
	}
	public String getAdresaC() {
		return adresaC.getText();
	}
	public String getEmailC() {
		return emailC.getText();
	}
	public String getVarstaC() {
		return varstaC.getText();
	}
	public String getIdP() {
		return idP.getText();
	}
	public String getNumeP() {
		return numeP.getText();
	}
	public String getCantitateP() {
		return cantitateP.getText();
	}
	public String getPretP() {
		return pretP.getText();
	}
	public String getPerisabilP() {
		return perisabilP.getText();
	}
	public Integer getComboP() {
		return (Integer)productsList.getSelectedItem();
	}
	public Integer getComboC() {
		return (Integer)clientsList.getSelectedItem();
	}public String getCantitate() {
		return cantitate.getText();
	}
	//JOptionPane;
	public void showMessage(String message) {
		JOptionPane.showMessageDialog(this,message);
	}
}
