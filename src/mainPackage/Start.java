package mainPackage;


import java.sql.SQLException;

import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.swing.JOptionPane;

import com.mysql.jdbc.exceptions.MySQLIntegrityConstraintViolationException;

import presentation.Controller;
import presentation.View;
import businessLayer.ClientBLL;
import businessLayer.ComandaBLL;
import businessLayer.ProdusBLL;
import dataAccessLayer.AbstractDAO;
import model.Client;
import model.Produs;


/**
 * @Author: Technical University of Cluj-Napoca, Romania Distributed Systems
 *          Research Laboratory, http://dsrl.coned.utcluj.ro/
 * @Since: Apr 03, 2017
 */
public class Start {
	protected static final Logger LOGGER = Logger.getLogger(Start.class.getName());

	public static void main(String[] args) throws SQLException {
		//AbstractDAO model=  new AbstractDAO();
		//StudentBLL studentBll = new StudentBLL();
		View v;
		ProdusBLL produsBll= new ProdusBLL();
		ClientBLL clientBll= new ClientBLL();
		ComandaBLL comandaBll=new ComandaBLL();
		 v=new View(produsBll,clientBll);
		Controller c=new Controller(clientBll, produsBll, comandaBll, v);}
		
		//Student student1 = null;
	
		//Produs ins= new Produs(6,"mazare", 20, 50, "da");
		//produsBll.delete(ins);
		//prod=produsBll.findAll();
		// obtain field-value pairs for object through reflection
//		ReflectionExample.retrieveProperties(p);
		//for (Produs p1:prod) {
//			System.out.println(p1.toString());
			//ReflectionExample.retrieveProperties(p1);
		//}
	
}
