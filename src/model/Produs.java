package model;

public class Produs {
	private int id;
	private String nume;
	private int cantitate;
	private int pret;
	private String perisabil;
	
	public Produs() {
		
	}
	public Produs( String nume, int cantitate, int pret, String perisabil) {
		super();
		this.nume=nume;
		this.cantitate=cantitate;
		this.pret=pret;
		this.perisabil=perisabil;
	}
	public Produs(int idProdus, String nume, int cantitate, int pret, String perisabil) {
		super();
		this.id = idProdus;
		this.nume = nume;
		this.cantitate = cantitate;
		this.pret = pret;
		this.perisabil = perisabil;
	}
	@Override
	public String toString() {
		return "Produs[id="+id+", nume="+nume+", cantitate="+cantitate+", pret="+pret+", perisabil="+perisabil+"]";
	}
	public int getId() {
		return id;
	}
	public void setId(int idProdus) {
		this.id = idProdus;
	}
	public String getNume() {
		return nume;
	}
	public void setNume(String nume) {
		this.nume = nume;
	}
	public int getCantitate() {
		return cantitate;
	}
	public void setCantitate(int cantitate) {
		this.cantitate = cantitate;
	}
	public int getPret() {
		return pret;
	}
	public void setPret(int pret) {
		this.pret = pret;
	}
	public String getPerisabil() {
		return perisabil;
	}
	public void setPerisabil(String perisabil) {
		this.perisabil = perisabil;
	}
	

}
