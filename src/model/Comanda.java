package model;

public class Comanda {
	private int id;
	private String client;
	private String produs;
	private int cantitate;
	@Override
	public String toString() {
		return "Comanda [id="+id+", client="+client+", produs="+produs+", cantitate="+cantitate+"];";
	}
	public Comanda() {
		
	}
	public Comanda(int id, String client, String produs, int cantitate) {
		super();
		this.id = id;
		this.client = client;
		this.produs = produs;
		this.cantitate = cantitate;
	}
	public Comanda(String client, String produs, int cantitate) {
		this.client=client;
		this.produs=produs;
		this.cantitate=cantitate;
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getClient() {
		return client;
	}
	public void setClient(String client) {
		this.client = client;
	}
	public String getProdus() {
		return produs;
	}
	public void setProdus(String produs) {
		this.produs = produs;
	}
	public int getCantitate() {
		return cantitate;
	}
	public void setCantitate(int cantitate) {
		this.cantitate = cantitate;
	}
	

}
