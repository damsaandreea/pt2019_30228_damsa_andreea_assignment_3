package dataAccessLayer;

import java.beans.IntrospectionException;
import java.beans.PropertyDescriptor;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.lang.reflect.ParameterizedType;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import com.mysql.jdbc.exceptions.MySQLIntegrityConstraintViolationException;

import connection.ConnectionFactory;

/**
 * @Author: Technical University of Cluj-Napoca, Romania Distributed Systems
 *          Research Laboratory, http://dsrl.coned.utcluj.ro/
 * @Since: Apr 03, 2017
 * @Source http://www.java-blog.com/mapping-javaobjects-database-reflection-generics
 */

public class AbstractDAO<T> {
	protected static final Logger LOGGER = Logger.getLogger(AbstractDAO.class.getName());
	private final Class<T> type;
	@SuppressWarnings("unchecked")
	public AbstractDAO() {
		this.type = (Class<T>) ((ParameterizedType) getClass().getGenericSuperclass()).getActualTypeArguments()[0];
	}
	private String createSelectQuery(String field) {
		StringBuilder sb = new StringBuilder();
		sb.append("SELECT ");
		sb.append(" * ");
		sb.append(" FROM ");
		sb.append(type.getSimpleName());
		sb.append(" WHERE " + field + " =?");
		return sb.toString();
	}
	private String createSelectAllQuery() {
		StringBuilder sb = new StringBuilder();
		sb.append("SELECT ");
		sb.append(" * ");
		sb.append(" FROM ");
		sb.append(type.getSimpleName());	
		return sb.toString();
	}
	private String createInsertQuery(Object obj) {
		StringBuilder sb=new StringBuilder();
		try {
		T instance = type.newInstance();
		sb.append("INSERT INTO ");
		sb.append(type.getSimpleName());
		sb.append(" (");
		String prefix="";
		for (Field field : type.getDeclaredFields()) {
			String s=field.getName();
			sb.append(prefix);
			prefix=", ";
			sb.append(s);
		}
		sb.append(")");
		sb.append(" VALUES (");
		prefix="";
		for (Field field: type.getDeclaredFields()) {
			field.setAccessible(true);
			Object value = field.get(obj);
			String element="'"+value+"'";
			sb.append(prefix);
			prefix=", ";
			sb.append(element);
		}
		sb.append(")");
		}catch (Exception e) {
			e.printStackTrace();
		} 
		return sb.toString();
	}
	private String createUpdateQuery(Object obj) {
		StringBuilder sb= new StringBuilder();
		sb.append("UPDATE ");
		sb.append(type.getSimpleName());
		sb.append(" SET ");
		String prefix="";
		int id=1;
		try {
		for (Field field: type.getDeclaredFields()) {
			field.setAccessible(true);
			Object value=field.get(obj);
			if (field.getName().equals("id")) {
				id=(int)value;}
			if (!field.getName().equals("id")) {
			String element="'"+value+"'";
			sb.append(prefix);
			prefix=", ";
			sb.append(field.getName());
			sb.append("=");
			sb.append(element);
			}
		}
		sb.append(" WHERE id=");
		sb.append(id);
		}catch(Exception e) {
			e.printStackTrace();
		}
		return sb.toString();
	}
	
	public String createDeleteQuery(Object obj) {
		StringBuilder sb=new StringBuilder();
		sb.append("DELETE FROM ");
		sb.append(type.getSimpleName());
		sb.append(" WHERE id=");
		int id=1;
		try {
		for (Field field: type.getDeclaredFields()) {
			field.setAccessible(true);
			Object value=field.get(obj);
			if (field.getName().equals("id")) {
				id=(int)value;}
		}}catch(Exception e) {
			e.printStackTrace();}
		sb.append(id);
		return sb.toString();
	}

	public List<T> findAll() {
		Connection connection = null;
		PreparedStatement statement = null;
		ResultSet resultSet = null;
		String query=createSelectAllQuery();
		//System.out.println(query);
		try {
			connection = ConnectionFactory.getConnection();
			statement = connection.prepareStatement(query);
			resultSet=statement.executeQuery();
//			while (resultSet.next()) {
//				System.out.println(resultSet.getInt(1));
//			}
			return createObjects(resultSet);
		}catch(SQLException e) {
			LOGGER.log(Level.WARNING,type.getName()+"DAO:findALL "+ e.getMessage());
		}
		finally {
		ConnectionFactory.close(resultSet);
		ConnectionFactory.close(statement);
		ConnectionFactory.close(connection);
		}
		return null;
	}
	public T findById(int id) {
		Connection connection = null;
		PreparedStatement statement = null;
		ResultSet resultSet = null;
		String query = createSelectQuery("id");
		try {
			connection = ConnectionFactory.getConnection();
			statement = connection.prepareStatement(query);
			statement.setInt(1, id);
			resultSet = statement.executeQuery();
			//System.out.println(resultSet.getInt(1));
			return createObjects(resultSet).get(0);
		} catch (SQLException e) {
			LOGGER.log(Level.WARNING, type.getName() + "DAO:findById " + e.getMessage());
		} finally {
			ConnectionFactory.close(resultSet);
			ConnectionFactory.close(statement);
			ConnectionFactory.close(connection);
		}
		return null;}

	private List<T> createObjects(ResultSet resultSet) {
		List<T> list = new ArrayList<T>();

		try {
			while (resultSet.next()) {
				T instance = type.newInstance();
				for (Field field : type.getDeclaredFields()) {
					Object value = resultSet.getObject(field.getName());
					PropertyDescriptor propertyDescriptor = new PropertyDescriptor(field.getName(), type);
					Method method = propertyDescriptor.getWriteMethod();
					method.invoke(instance, value);
				}
				list.add(instance);
				}
		} catch (Exception e) {
			e.printStackTrace();} 
		return list;}

	public T insert(T t) {
	
		Connection connection = null;
		PreparedStatement statement = null;
		String query=createInsertQuery(t);
		try {
			connection = ConnectionFactory.getConnection();
			statement = connection.prepareStatement(query);
			statement.executeUpdate();
		}catch (Exception e) {
			e.printStackTrace();}
		
		return t;}

	public T update(T t) {
		Connection connection = null;
		PreparedStatement statement = null;
		String query=createUpdateQuery(t);
		try {
			connection = ConnectionFactory.getConnection();
			statement = connection.prepareStatement(query);
			statement.executeUpdate();
		}catch(Exception e) {
			e.printStackTrace();}
		return t;}
	public T delete(T t) {
		Connection connection = null;
		PreparedStatement statement = null;
		String query=createDeleteQuery(t);
		//System.out.println(query);
		try {
			connection = ConnectionFactory.getConnection();
			statement = connection.prepareStatement(query);
			statement.executeUpdate();
			}catch(Exception e) {
				e.printStackTrace();}
		return t;}
}
